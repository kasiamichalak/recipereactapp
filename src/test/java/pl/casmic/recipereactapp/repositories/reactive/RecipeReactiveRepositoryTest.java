package pl.casmic.recipereactapp.repositories.reactive;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.casmic.recipereactapp.models.Recipe;

@ExtendWith(SpringExtension.class)
@DataMongoTest
public class RecipeReactiveRepositoryTest {

    public static final String RAVIOLI = "Ravioli";
    @Autowired
    RecipeReactiveRepository recipeReactiveRepository;

    @BeforeEach
    public void setup() {
        recipeReactiveRepository.deleteAll().block();
    }

    @Test
    public void testSaveCategory() {

        Recipe recipe = new Recipe();
        recipe.setDescription(RAVIOLI);
        recipeReactiveRepository.save(recipe).block();

        Long actualCount = recipeReactiveRepository.count().block();
        Long expectedCount = Long.valueOf(1);

        Assertions.assertEquals(expectedCount, actualCount);
    }
}
