package pl.casmic.recipereactapp.repositories.reactive;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.casmic.recipereactapp.models.UnitOfMeasure;

@ExtendWith(SpringExtension.class)
@DataMongoTest
public class UnitOfMeasureReactiveRepositoryTest {

    public static final String LITER = "Liter";
    @Autowired
    UnitOfMeasureReactiveRepository uomReactiveRepository;

    @BeforeEach
    public void setup() {
        uomReactiveRepository.deleteAll().block();
    }

    @Test
    public void testSaveUom() throws Exception {

        UnitOfMeasure uom = new UnitOfMeasure();
        uom.setDescription(LITER);
        uomReactiveRepository.save(uom).block();
        Long actualCount = uomReactiveRepository.count().block();
        Long expectedCount = Long.valueOf(1);

        Assertions.assertEquals(expectedCount, actualCount);
    }

    @Test
    public void testFindUomByDescription() {

        UnitOfMeasure uom = new UnitOfMeasure();
        uom.setDescription(LITER);
        uomReactiveRepository.save(uom).block();

        UnitOfMeasure fetchedUom = uomReactiveRepository.findByDescription(LITER).block();

        Assertions.assertNotNull(fetchedUom.getId());
    }
}
