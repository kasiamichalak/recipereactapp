package pl.casmic.recipereactapp.repositories.reactive;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.casmic.recipereactapp.models.Category;

@ExtendWith(SpringExtension.class)
@DataMongoTest
public class CategoryReactiveRepositoryTest {

    public static final String POLISH = "Polish";
    @Autowired
    CategoryReactiveRepository categoryReactiveRepository;

    @BeforeEach
    public void setup() {
        categoryReactiveRepository.deleteAll().block();
    }

    @Test
    public void testSaveCategory() {

        Category category = new Category();
        category.setDescription(POLISH);
        categoryReactiveRepository.save(category).block();

        Long actualCount = categoryReactiveRepository.count().block();
        Long expectedCount = Long.valueOf(1);

        Assertions.assertEquals(expectedCount, actualCount);
    }

    @Test
    public void testFindCategoryByDescription() {

        Category category = new Category();
        category.setDescription(POLISH);
        categoryReactiveRepository.save(category).then().block();

        Category fetchedCategory = categoryReactiveRepository.findByDescription(POLISH).block();

        Assertions.assertNotNull(fetchedCategory.getId());
    }
}
