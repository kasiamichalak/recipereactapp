package pl.casmic.recipereactapp.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pl.casmic.recipereactapp.converters.RecipeCommandToRecipe;
import pl.casmic.recipereactapp.converters.RecipeToRecipeCommand;
import pl.casmic.recipereactapp.exceptions.NotFoundException;
import pl.casmic.recipereactapp.models.Recipe;
import pl.casmic.recipereactapp.repositories.RecipeRepository;
import pl.casmic.recipereactapp.repositories.reactive.RecipeReactiveRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class RecipeServiceImplTest {

    RecipeServiceImpl recipeService;
    @Mock
    RecipeReactiveRepository recipeReactiveRepository;
    @Mock
    RecipeCommandToRecipe recipeCommandToRecipe;
    @Mock
    RecipeToRecipeCommand recipeToRecipeCommand;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        recipeService = new RecipeServiceImpl(
                recipeReactiveRepository,
                recipeCommandToRecipe,
                recipeToRecipeCommand);
    }

    @Test
    void getRecipesTest() throws Exception {
//        given
        Recipe recipe = new Recipe();

        Mockito.when(recipeService.getRecipes()).thenReturn(Flux.just(recipe));
//        when
        List<Recipe> recipes = recipeService.getRecipes().collectList().block();
//        then
        assertEquals(recipes.size(), 1);
        verify(recipeReactiveRepository, times(1)).findAll();
        verify(recipeReactiveRepository, never()).findById(anyString());
    }
    @Test
    public void getRecipeByIdTest() throws Exception {
        Recipe recipe = new Recipe();
        recipe.setId("1");

        when(recipeReactiveRepository.findById(anyString())).thenReturn(Mono.just(recipe));

        Recipe recipeReturned = recipeService.findById("1").block();

        Assertions.assertNotNull(recipeReturned,"Null recipe returned");
        verify(recipeReactiveRepository, times(1)).findById(anyString());
        verify(recipeReactiveRepository, never()).findAll();
    }
    @Test
    public void testDeleteById() throws Exception {

        //given
        String idToDelete = "2";

        when(recipeReactiveRepository.deleteById(anyString())).thenReturn(Mono.empty());

        //when
        recipeService.deleteById(idToDelete).block();

        //then
        verify(recipeReactiveRepository, times(1)).deleteById(anyString());
    }
}