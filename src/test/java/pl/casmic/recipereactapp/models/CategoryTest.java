package pl.casmic.recipereactapp.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CategoryTest {

    Category category;
    
    @BeforeEach
    public void setUp() {
        category = new Category();
    }
    @Test
    void getId() {
//      given
        String expectedIdValue = "4";
        category.setId(expectedIdValue);
//      when & then
        assertEquals(expectedIdValue, category.getId());
    }
}