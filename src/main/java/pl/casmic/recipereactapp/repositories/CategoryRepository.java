package pl.casmic.recipereactapp.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.casmic.recipereactapp.models.Category;

import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, String> {

    Optional<Category> findByDescription(String description);
}
