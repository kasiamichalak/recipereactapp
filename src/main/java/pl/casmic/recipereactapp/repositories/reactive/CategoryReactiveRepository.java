package pl.casmic.recipereactapp.repositories.reactive;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import pl.casmic.recipereactapp.models.Category;
import reactor.core.publisher.Mono;

public interface CategoryReactiveRepository extends ReactiveMongoRepository<Category, String> {

    Mono<Category> findByDescription(String description);
}
