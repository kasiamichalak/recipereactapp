package pl.casmic.recipereactapp.repositories.reactive;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import pl.casmic.recipereactapp.models.UnitOfMeasure;
import reactor.core.publisher.Mono;

public interface UnitOfMeasureReactiveRepository extends ReactiveMongoRepository<UnitOfMeasure, String> {

    Mono<UnitOfMeasure> findByDescription(String description);
}
