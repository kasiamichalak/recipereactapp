package pl.casmic.recipereactapp.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.casmic.recipereactapp.models.UnitOfMeasure;

import java.util.Optional;

public interface UnitOfMeasureRepository extends CrudRepository<UnitOfMeasure, String> {

    Optional<UnitOfMeasure> findByDescription(String description);
}
