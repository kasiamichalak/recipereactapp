package pl.casmic.recipereactapp.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.casmic.recipereactapp.models.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, String> {

}
