package pl.casmic.recipereactapp.services;

import org.springframework.stereotype.Service;
import pl.casmic.recipereactapp.commands.UnitOfMeasureCommand;
import pl.casmic.recipereactapp.converters.UnitOfMeasureToUnitOfMeasureCommand;
import pl.casmic.recipereactapp.repositories.UnitOfMeasureRepository;
import pl.casmic.recipereactapp.repositories.reactive.UnitOfMeasureReactiveRepository;
import reactor.core.publisher.Flux;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UnitOfMeasureServiceImp implements UnitOfMeasureService {

    private final UnitOfMeasureReactiveRepository unitOfMeasureReactiveRepository;
    private final UnitOfMeasureToUnitOfMeasureCommand unitOfMeasureToUnitOfMeasureCommand;

    public UnitOfMeasureServiceImp(UnitOfMeasureReactiveRepository unitOfMeasureReactiveRepository,
                                   UnitOfMeasureToUnitOfMeasureCommand unitOfMeasureToUnitOfMeasureCommand) {
        this.unitOfMeasureReactiveRepository = unitOfMeasureReactiveRepository;
        this.unitOfMeasureToUnitOfMeasureCommand = unitOfMeasureToUnitOfMeasureCommand;
    }

    @Override
    public Flux<UnitOfMeasureCommand> findAll() {
        return unitOfMeasureReactiveRepository.findAll()
                .map(unitOfMeasureToUnitOfMeasureCommand::convert);
    }
}
