package pl.casmic.recipereactapp.services;

import pl.casmic.recipereactapp.commands.UnitOfMeasureCommand;
import reactor.core.publisher.Flux;

public interface UnitOfMeasureService {

    Flux<UnitOfMeasureCommand> findAll();
}
