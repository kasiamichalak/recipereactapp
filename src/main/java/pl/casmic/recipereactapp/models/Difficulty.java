package pl.casmic.recipereactapp.models;

public enum Difficulty {
    EASY,
    MODERATE,
    KIND_OF_HARD,
    HARD
}
