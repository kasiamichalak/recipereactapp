package pl.casmic.recipereactapp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import pl.casmic.recipereactapp.commands.IngredientCommand;
import pl.casmic.recipereactapp.commands.RecipeCommand;
import pl.casmic.recipereactapp.commands.UnitOfMeasureCommand;
import pl.casmic.recipereactapp.services.IngredientService;
import pl.casmic.recipereactapp.services.RecipeService;
import pl.casmic.recipereactapp.services.UnitOfMeasureService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Controller
public class IngredientController {

    private static final String RECIPE_INGREDIENT_FORM_URL = "recipe/ingredient/form";
    private final RecipeService recipeService;
    private final IngredientService ingredientService;
    private final UnitOfMeasureService unitOfMeasureService;
    private WebDataBinder webDataBinder;

    public IngredientController(RecipeService recipeService, IngredientService ingredientService, UnitOfMeasureService unitOfMeasureService) {
        this.recipeService = recipeService;
        this.ingredientService = ingredientService;
        this.unitOfMeasureService = unitOfMeasureService;
    }

    @InitBinder("ingredient")
    public void initBinder (WebDataBinder webDataBinder) {
        this.webDataBinder = webDataBinder;
    }

    @GetMapping("recipe/{id}/ingredients")
    public String displayRecipeAllIngredients(@PathVariable String id, Model model) {
        model.addAttribute("recipe", recipeService.findCommandById(id));
        return "recipe/ingredient/list";
    }

    @GetMapping("recipe/{recipeId}/ingredient/{ingredientId}/show")
    public String displayIngredient(@PathVariable String recipeId,
                                    @PathVariable String ingredientId, Model model) {
        model.addAttribute("ingredient",
                                    ingredientService.findByRecipeIdAndIngredientId(recipeId, ingredientId));
        return "recipe/ingredient/show";
    }

    @GetMapping("recipe/{recipeId}/ingredient/new")
    public String displayNewIngredientForm(@PathVariable String recipeId, Model model) {
        //make sure we have a good id value
        Mono<RecipeCommand> recipeCommand = recipeService.findCommandById(recipeId);
        //todo raise exception if null

        //need to return back parent id for hidden form property
        IngredientCommand ingredientCommand = new IngredientCommand();
        ingredientCommand.setRecipeId(recipeId);
        model.addAttribute("ingredient", ingredientCommand);

        //init unitOfMeasure
        ingredientCommand.setUnitOfMeasure(new UnitOfMeasureCommand());
        return RECIPE_INGREDIENT_FORM_URL;
    }

    @GetMapping("recipe/{recipeId}/ingredient/{ingredientId}/update")
    public String displayUpdateIngredientForm(@PathVariable String recipeId,
                                              @PathVariable String ingredientId, Model model) {
        model.addAttribute("ingredient",
                ingredientService.findByRecipeIdAndIngredientId(recipeId, ingredientId));
        return RECIPE_INGREDIENT_FORM_URL;
    }

    @PostMapping("recipe/{recipeId}/ingredient")
    public String saveOrUpdateIngredient(@ModelAttribute ("ingredient") IngredientCommand ingredientCommand,
                                         @PathVariable String recipeId, Model model) {
        webDataBinder.validate();
        BindingResult bindingResult = webDataBinder.getBindingResult();

        if(bindingResult.hasErrors()) {
            bindingResult.getAllErrors()
                    .forEach(objectError -> log.debug(objectError.toString()));
            return RECIPE_INGREDIENT_FORM_URL;
        }
        IngredientCommand savedIngredientCommand = ingredientService.saveIngredientCommand(ingredientCommand).block();
        return "redirect:/recipe/" + recipeId + "/ingredient/" + savedIngredientCommand.getId() + "/show";
    }

    @GetMapping("/recipe/{recipeId}/ingredient/{ingredientId}/delete")
    public String deleteIngredient(@PathVariable String recipeId,
                                   @PathVariable String ingredientId) {
        ingredientService.deleteByIngredientId(recipeId, ingredientId).block();
        return "redirect:/recipe/" + recipeId + "/ingredients";
    }

    @ModelAttribute("unitofmeasurelist")
    Flux<UnitOfMeasureCommand> populateUomList() {
        return unitOfMeasureService.findAll();
    }
}
